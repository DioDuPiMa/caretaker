caretaker
=========

This tool will provide above all a fast way to check on the status of your remote self-hosted machines. There are plenty of server management tools out there, like Cockpit or Cpanel, however, it is not this app purpose to replace them, because it will not have its powerful tools, instead it is a fast way to check if your server are online, and if not turn them on on WoL or Off.


Roadmap
_______

-  0.1
	- Single Companion
		- Name
		- User
		- IP (4/6)
		- MAC
		- Gateway IP (4/6)
	- Manage Status
		- Wake Up (WoL)
		- Restart (ssh)
		- Shutdown (ssh)
	- Status Data
		- ping every sec and check if online or not
		- time since last wake up
		- last wake up time
		- other health checks ???
	- Ubuntu TaskBar widget
- 0.2
	- Windows / Mac Version (OS aware style)
		- OS specific taskbar integration?
	- 18i language support
	- self check in first connection if WoL is ready on remote device
- 0.3 The Dawn of Multi Device
	- List of servers
		- Name
		- User
		- IP (4/6)
		- MAC
		- Gateway IP (4/6)
		- Description
		- Mgnt Interface
		- Status Data
			- ping every sec and check if online or not
			- time since last wake up
			- last wake up time
			- other health checks ???
- 0.4
	- Implement own WoL Library
	- Implement own Ping library (TCP, UDP, ICMP)
	- IPMI managment
- 0.5 "Behind Closed Walls"
	- Implement Companion Clusters
	- Implement VPN functionality (Wireguard)
-  1.0 Caretaker Hub
	- on-demand infrastructure management for self-hosting use
		- infrastructure as a service, as in linux OS service
		- infrastructure up-time scheduling
			- calendar view
			- list view