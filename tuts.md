# PyQT5
https://medium.com/analytics-vidhya/how-to-build-your-first-desktop-application-in-python-7568c7d74311
https://tutorialforlinux.com/2018/06/06/ubuntu-meteo-qt-widget-setup/
https://scribles.net/installing-the-latest-qt-creator-on-ubuntu-linux/
https://stackoverflow.com/questions/36606771/pyqt-how-do-i-update-a-label
https://stackoverflow.com/questions/40982518/argument-1-has-unexpected-type-nonetype
https://gist.github.com/Axel-Erfurt/5d72e4c80d6eb83d7af5404abd803845
https://fosspost.org/custom-system-tray-icon-indicator-linux/
https://stackoverflow.com/questions/49971584/updating-pyqt5-gui-with-live-data
https://pythonbasics.org/pyqt-qmessagebox/
https://www.tutorialspoint.com/pyqt/pyqt_qlistwidget.htm
https://www.learnpyqt.com/tutorials/widget-search-bar/
https://pythonbasics.org/qlineedit/
https://stackoverflow.com/questions/49971584/updating-pyqt5-gui-with-live-data

# WoL
https://stackoverflow.com/questions/28765352/wakeup-on-lan-with-python
https://andrewbridge.wordpress.com/2016/07/30/wake-on-lan-on-linux/
https://www.cyberciti.biz/tips/linux-send-wake-on-lan-wol-magic-packets.html
https://stackoverflow.com/questions/28765352/wakeup-on-lan-with-python
https://unix.stackexchange.com/questions/506582/how-to-capture-wol-packets
http://www.microhowto.info/howto/get_the_mac_address_of_an_ethernet_interface_in_c_using_siocgifhwaddr.html
https://engineerworkshop.com/blog/remote-start-for-servers-how-to-set-up-wake-on-lan/
https://wiki.alpinelinux.org/wiki/Configure_Wake-on-LAN
https://pibrewstuff.wordpress.com/2015/02/02/linux-wake-on-lan-wol-settings-ubuntu-14-04/

# Sockets and Nets
https://realpython.com/python-1sockets/1

# SSH
https://stackoverflow.com/questions/22587855/running-sudo-command-with-paramiko
https://stackoverflow.com/questions/35821184/implement-an-interactive-shell-over-ssh-in-python-using-paramiko/36948840#36948840
https://stackoverflow.com/questions/24463500/generating-ssh-keypair-with-paramiko-in-python
http://docs.paramiko.org/en/stable/api/keys.html?highlight=Send%20Key
https://gist.github.com/batok/2352501
https://stackoverflow.com/questions/2466401/how-to-generate-ssh-key-pairs-with-python
https://www.ssh.com/ssh/copy-id
https://duckduckgo.com/?t=canonical&q=ssh+wihtout+keys&ia=web
https://web.archive.org/web/20091220003139/http://jessenoller.com/2009/02/05/ssh-programming-with-paramiko-completely-different/
https://hackersandslackers.com/automate-ssh-scp-python-paramiko/

# Networks
https://superuser.com/questions/29640/inverse-arp-lookup
https://www.cyberciti.biz/faq/show-ethernet-adapter-ubuntu-linux/
https://askubuntu.com/questions/704361/why-is-my-network-interface-named-enp0s25-instead-of-eth0
https://askubuntu.com/questions/679117/predict-network-interfaces-names-in-wily-xenial/679141#679141

https://jamesoff.github.io/simplemonitor/