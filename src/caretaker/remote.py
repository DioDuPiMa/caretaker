from wakeonlan import send_magic_packet
import json
import os
from tcp_latency import measure_latency
from paramiko import SSHClient, AutoAddPolicy, RSAKey
import re
from pathlib import Path
import time


def create_conf(path):
    try:
        if not os.path.isdir(path):
            Path.mkdir(path)
            Path.mkdir(Path.joinpath(path, "remotes"))
            return path
    except OSError as error:
        return error


class Remote:
    def __init__(self):
        # Perm Param
        self.name = 'Default'
        self.ipv4 = '255.255.255.255'
        self.ipv6 = ''
        self.wol_port = 9
        self.ssh_port = 22
        self.mac = ''
        # self.ssh_key = ''
        self.username = ''
        # Session Param
        self.status = 0
        self.password = None
        self.conn = None
        self.client = None
        self.conf_path = Path.joinpath(Path.home(), ".caretaker")
        self.remotes_path = Path.joinpath(self.conf_path, "remotes")

    # TODO: Separate WoL Port from SSH Port

    def create(self):
        self.update()
        return self

    def update(self, name, username, ipv4, ipv6, wol_port, ssh_port, mac):
        self.name = name
        self.username = username
        self.ipv4 = ipv4
        self.ipv6 = ipv6
        self.wol_port = wol_port
        self.wol_port = ssh_port
        self.mac = mac
        # self.ssh_key = ssh_key
        self.save()
        # TODO: add validation for data
        # TODO: pass validation of int to remote obj
        return self

    def load(self, file=""):
        if not file:
            file = Path.joinpath(self.remotes_path, "main.json")
        if not os.path.isfile(file):
            self.save()
        f = open(file, )
        data = json.load(f)
        for param in data.keys():
            if param in self.__dict__:
                self.__dict__[param] = data[param]
        # TODO: Reject if information is not enough
        # A more restrictive implementation in term of what the file should contain once development requeriments stabilize

        # save new main remote
        # self.save()
        return self

    def save(self):
        # Path('/remotes/').mkdir(parents=True, exist_ok=True)
        create_conf(self.conf_path)
        with open(Path.joinpath(self.remotes_path, "main.json"), 'w') as json_file:
            json.dump({k: v for k, v in self.__dict__.items() if k not in ['conn', 'password', 'client', 'status', 'conf_path', 'remotes_path']},
                      json_file)
        # TODO: Overwriting protection
        # TODO: Correct permissions and folder "remotes"
        return self

    # TODO: Refactor SSH handling into its own class 'connection' so we can have modular conections for example SAMBA for Windows machines with diferent command sets
    # change
    # def _get_ssh_key(self):
    #	self.ssh_key = RSAKey.from_private_key_file(self.ssh_key)
    #	return self.ssh_key

    # change
    # def _upload_ssh_key(self):
    #	os.system(f'ssh-copy-id -i {self.ssh_key} {self.username}@{self.ipv4}>/dev/null 2>&1')
    #	os.system(f'ssh-copy-id -i {self.ssh_key}.pub {self.username}@{self.ipv4}>/dev/null 2>&1')

    # Change
    def _connect(self):
        if self.conn is None:
            self.client = SSHClient()
            self.client.load_system_host_keys()
            self.client.set_missing_host_key_policy(AutoAddPolicy())
            self.client.connect(
                self.ipv4,
                username=self.username,
                # key_filename=self.ssh_key,
                password=self.password,
                # look_for_keys=True,
                timeout=5000,
                port=self.ssh_port
            )
        return self.client

    # Change
    def _execute_command(self, command, sudo=False):
        self.conn = self._connect()
        if sudo is True:
            stdin, stdout, stderr = self.client.exec_command('sudo -S ' + command)
            stdin.write('{0}\n'.format(self.password))
            stdin.flush()
        else:
            stdin, stdout, stderr = self.client.exec_command(command)

    # Change
    def _disconnect(self):
        if self.client:
            self.client.close()

    def ping(self):
        # ICMP methods though a best practice require privilige escalation to root, which is not good for such a program
        # results = pythonping.ping(self.ipv4, count=1)
        ping = False
        if measure_latency(host=self.ipv4, runs=1)[0]:
            ping = True
        return ping

    def get_status(self):
        ping = self.ping()
        if ping:
            key = 1
        else:
            key = 2
        statuses = {0: 'Unknown', 1: 'Online', 2: 'Offline', }
        self.status = key
        return key, statuses.get(key, 'Unknown')

    def wake_up(self):
        send_magic_packet(self.mac)

    def restart(self):
        self._execute_command('shutdown --reboot now', sudo=True)
        self._disconnect()

    def power_off(self):
        self._execute_command('shutdown -P now', sudo=True)
        self._disconnect()


# def keygen(self, filename=None, passwd=None, bits=1024):
# 		if filename is None:
# 			filename = self.name
# 		k = RSAKey.generate(bits)
# 		# This line throws the error.
# 		k.write_private_key_file(filename, password=passwd)
# 		o = open(Path.joinpath(Path.home(), '.ssh', filename.lower() + '.pub'), "w").write(k.get_base64())


# TODO: ADD sqllite for backend, as it allows logging changes in both details, aswell as status and command executions
if __name__ == '__main__':
    a = Remote()
    a.load("remotes/main.json")
    print(a.__dict__)
    print(a.ping())
    a.save()
