import os
import sys

from PyQt5.QtGui import QGuiApplication
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout, QLabel, QGroupBox, QLineEdit, QMainWindow, \
	QFileDialog, QInputDialog, QErrorMessage, QMessageBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot, QTimer
from caretaker.remote import Remote
from pathlib import Path


class MainWindow(QMainWindow):
	def __init__(self):
		super(MainWindow, self).__init__()
		# Load default remote
		self.main_remote = Remote()
		# check if main exists or throw up a open file dialog/ create new
		self.main_remote.load()

		self.resize(400, 400)  # The resize() method resizes the widget.
		self.setWindowTitle("Caretaker")

		window = QWidget(self)
		self.setCentralWidget(window)
		layout = QVBoxLayout()

		# Server/Remote
		self.server_window = QGroupBox()
		self.server_window.setTitle('Remote')
		self.server_layout = QVBoxLayout()

		self.server_layout.addWidget(QLabel('Name'))
		self.name = QLineEdit(self.main_remote.name)
		self.server_layout.addWidget(self.name)
		self.server_layout.addWidget(QLabel('Username'))
		self.username = QLineEdit(self.main_remote.username)
		self.server_layout.addWidget(self.username)
		self.server_layout.addWidget(QLabel('MAC Address'))
		self.mac = QLineEdit(self.main_remote.mac)
		self.server_layout.addWidget(self.mac)
		self.server_layout.addWidget(QLabel('IPv4'))
		self.ipv4 = QLineEdit(self.main_remote.ipv4)
		self.server_layout.addWidget(self.ipv4)
		self.server_layout.addWidget(QLabel('IPv6'))
		self.ipv6 = QLineEdit(self.main_remote.ipv6)
		self.server_layout.addWidget(self.ipv6)

		# TODO: SSH keyfiles feature
		#self.server_layout.addWidget(QLabel('SSH Key'))
		#self.ssh_key = QLineEdit(self.main_remote.ssh_key)
		#self.server_layout.addWidget(self.ssh_key)
		#self.ssh_key_dialog = QPushButton('Choose SSH Key', clicked=lambda: self.get_ssh_key())
		#self.ssh_key_generate_dialog = QPushButton('Generate new SSH Key', clicked=lambda: self.get_ssh_key())
		#self.server_layout.addWidget(self.ssh_key_dialog)
		#self.server_layout.addWidget(self.ssh_key_generate_dialog)

		self.server_layout.addWidget(QLabel('WoL Port'))
		self.wol_port = QLineEdit(str(self.main_remote.wol_port))
		self.server_layout.addWidget(self.wol_port)
		self.server_layout.addWidget(QLabel('SSH Port'))
		self.ssh_port = QLineEdit(str(self.main_remote.ssh_port))
		self.server_layout.addWidget(self.ssh_port)

		self.server_layout.addWidget(QPushButton('Load', clicked=lambda: self.load_remote()))
		self.server_layout.addWidget(QPushButton('Save', clicked=lambda: self.save_remote()))
		self.server_layout.addWidget(QPushButton('Reset', clicked=lambda: self.reset_remote()))
		self.server_window.setLayout(self.server_layout)

		# Status
		self.status_window = QGroupBox()
		self.status_window.setTitle('Power Status')
		self.status_layout = QVBoxLayout()

		self.status = QLabel("Pending")
		self.check_status()
		self.status_layout.addWidget(self.status)
		self.status_layout.addWidget(QPushButton('Power On', clicked=lambda: self.poweron_remote()))
		self.status_layout.addWidget(QPushButton('Power Off', clicked=lambda: self.poweroff_remote()))
		self.status_layout.addWidget(QPushButton('Restart', clicked=lambda: self.restart_remote()))
		self.status_window.setLayout(self.status_layout)

		# Layout
		layout.addWidget(self.server_window)
		layout.addWidget(self.status_window)
		window.setLayout(layout)

	def save_remote(self):
		self.main_remote.name = self.name.text()
		self.main_remote.username = self.username.text()
		self.main_remote.mac = self.mac.text()
		self.main_remote.ipv4 = self.ipv4.text()
		self.main_remote.ipv6 = self.ipv6.text()
		self.main_remote.wol_port = self.wol_port.text()
		self.main_remote.ssh_port = self.ssh_port.text()
		#self.main_remote.ssh_key = self.ssh_key.text()
		self.main_remote.save()

	def update_gui_remote(self, remote):
		self.name.setText(self.main_remote.name)
		self.username.setText(self.main_remote.username)
		self.mac.setText(self.main_remote.mac)
		self.ipv4.setText(self.main_remote.ipv4)
		self.ipv6.setText(self.main_remote.ipv6)
		self.wol_port.setText(str(self.main_remote.wol_port))
		self.ssh_port.setText(str(self.main_remote.ssh_port))
		#self.ssh_key.setText(self.main_remote.ssh_key)

	def load_remote(self):
		try:
			fname = QFileDialog.getOpenFileName(self, caption='Remote Config file', directory=str(Path.home()))
			self.temp_remote = Remote().load(file=fname[0])
			self.update_gui_remote(self.temp_remote)
		except:
			pass

	def reset_remote(self):
		self.main_remote.load()
		self.update_gui_remote(self.main_remote)

	def password_dialog(self):
		if not self.main_remote.password:
			text, okPressed = QInputDialog.getText(self, "Get Password", "{0} password:".format(self.main_remote.username), QLineEdit.Normal, "")
			if okPressed and text != '':
				self.main_remote.password = text

	def poweron_remote(self):
		if self.main_remote.status != 1:
			self.main_remote.wake_up()
			exec_dialog = QMessageBox()
			exec_dialog.setText('Command Executed')
			exec_dialog.exec()
		else:
			error_dialog = QErrorMessage()
			error_dialog.showMessage('{0} is already Online'.format(self.main_remote.name))
			error_dialog.exec_()

	def poweroff_remote(self):
		if self.main_remote.status == 1:
			try:
				self.password_dialog()
				self.main_remote.power_off()
				exec_dialog = QMessageBox()
				exec_dialog.setText('Command Executed')
				exec_dialog.exec()
			except:
				error_dialog = QErrorMessage()
				error_dialog.showMessage('Connection Error')
				error_dialog.exec_()
		else:
			error_dialog = QErrorMessage()
			error_dialog.showMessage('{0} is not Online'.format(self.main_remote.name))
			error_dialog.exec_()

	def restart_remote(self):
		if self.main_remote.status == 1:
			try:
				self.password_dialog()
				self.main_remote.restart()
				exec_dialog = QMessageBox()
				exec_dialog.setText('Command Executed')
				exec_dialog.exec()
			except:
				error_dialog = QErrorMessage()
				error_dialog.showMessage('Connection Error')
				error_dialog.exec_()
		else:
			error_dialog = QErrorMessage()
			error_dialog.showMessage('{0} is not Online'.format(self.main_remote.name))
			error_dialog.exec_()
	"""
	def get_ssh_key(self):
		fname = QFileDialog.getOpenFileName(self, caption='SSH Key File', directory='{0}/.ssh'.format(Path.home()))
		self.ssh_key.setText(fname[0])
		self.main_remote.ssh_key = fname[0]
	"""

	def check_status(self):
		self.status.setText(self.main_remote.get_status()[1])


def main():
	app = QApplication([])
	app.setStyle('Fusion')
	mainWin = MainWindow()
	mainWin.show()
	timer = QTimer()
	timer.timeout.connect(mainWin.check_status)
	timer.start(10000)  # every 10,000 milliseconds
	exit(app.exec_())


if __name__ == '__main__':
	sys.exit(main())

# TODO: Maybe Create connection object ?
