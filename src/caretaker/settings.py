import sys
from pathlib import Path


def read_txt(path):
    """
    Read txt key-value pair formated files
    """
    with open(path, "r") as wfile:
        return {content[0]: content[1].rstrip() for line in wfile.readlines() if len(content := line.split(sep=" = ")) == 2}


def create_desktop(directory=""):
    if not directory:
        directory = Path.joinpath(Path.home(), ".local", "share", "applications")
    with open(Path.joinpath(Path(directory), "caretaker.desktop"), "w") as desktop_file:
        instalation_dir = Path(sys.argv[0]).parents[0]
        egg_directory = Path.joinpath(instalation_dir.parents[0], "caretaker.egg-info", "entry_points.txt")
        entry_point = Path(read_txt(egg_directory).get("caretaker"))
        content = ["[Desktop Entry]",
                   "Name[en_US]=Caretaker",
                   f'Exec="{entry_point}"',
                   f"Path={instalation_dir}",
                   # "Icon=",
                   "Terminal = false",
                   "Type = Application",
                   "Categories = Application"]

        desktop_file.writelines([str(line) + "\n" for line in content])


if __name__ == '__main__':
    create_desktop()